# in programming a variable is nothing more than a name for something so you can use the name in code rather
# than the something.

card=10

card_name="Jack"

total_card=52

print(card)
print(card_name)
print(total_card)