'''
    In python, there are many math symbols:

        + plus
        - minus
        / divide
        // floor division
        % modulo (remainder)
        * multiply
        ** power
        < less-than
        > greater-than
        <= less-than-or-equal-to
        >= greater-than-or-equal-to

        some examples:
'''
print(3+5)
print(5-3)
print(3*4)
print(2**2)
print(7/2)
print(9//2)
print(8%3)
print(8<7)
print(8<=9)
print(9>10)
print(9>=7)