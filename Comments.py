# for Single line Comment use "#", for ex:

#Hey its a single Line Comment

# and for multi line comment use ''' the comments ''' for ex:

'''
    Hi it's a 
        multi line
            comment.
    Hope you understand this.
'''

#Coments are used for readability and they are ignored by compiler or interpretor.